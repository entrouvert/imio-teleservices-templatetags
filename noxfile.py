import shlex
from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=3.2,<3.3', use_develop=False):
    packages = [
        'Pillow',
        'Quixote>=3.0,<3.2',
        'WebTest',
        'bleach[css]<6',
        'django-ratelimit<3',
        'dnspython',
        'docutils',
        'emoji',
        'freezegun',
        'gadjo',
        'git+https://git.entrouvert.org/entrouvert/debian-django-ckeditor.git',
        'git+https://git.entrouvert.org/entrouvert/godo.js.git',
        'git+https://git.entrouvert.org/entrouvert/wcs.git',
        'langdetect',
        'lxml',
        'phonenumbers',
        'pre-commit',
        'psutil',
        'psycopg2-binary',
        'pyproj',
        'pyquery',
        'pytest',
        'pytest-cov',
        'pytest-django',
        'python-magic',
        'pyzbar',
        'qrcode',
        'requests',
        'unidecode',
        'vobject',
        'workalendar',
        f'django{django_version}',
        *packages,
    ]

    run_hook('setup_venv', session, packages)
    if use_develop:
        packages = ['-e', '.'] + packages
    else:
        packages = ['.'] + packages

    session.install(*packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize('django', ['>=3.2,<3.3', '>=4.2,<4.3'])
def tests(session, django):
    args = ['py.test']
    use_develop = False
    if '--coverage' in session.posargs or not session.interactive:
        use_develop = True
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov=imio_teleservices_templatetags/',
            '-v',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]
    args += session.posargs + ['tests/']

    setup_venv(session, django_version=django, use_develop=use_develop)

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'wcs.settings',
            'SETUPTOOLS_USE_DISTUTILS': 'stdlib',
            'WCS_SETTINGS_FILE': 'tests/settings.py',
        },
    )

    hookable_run(session, 'pre-commit', 'run', 'black', '--all-files', '--show-diff-on-failure')
