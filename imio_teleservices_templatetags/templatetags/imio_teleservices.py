import datetime
import json
import random
import re
from decimal import Decimal

import requests
from dateutil.relativedelta import relativedelta
from django import template
from django.utils.safestring import SafeText
from wcs.qommon import misc
from wcs.qommon.templatetags.qommon import unlazy
from wcs.variables import FlexibleDateObject, LazyFieldVarDate

register = template.Library()


def unlazy_date(fdate):
    if isinstance(fdate, (FlexibleDateObject, LazyFieldVarDate)):
        return str(unlazy(fdate))
    else:
        return fdate


@register.filter
def unlazy_filter(value):
    if isinstance(value, FlexibleDateObject):
        return str(unlazy(value))
    else:
        return unlazy(value)


@register.filter
def clean_string(value):
    value = unlazy_filter(value)
    return value.replace(' ', '').replace('.', '').replace('-', '')


@register.filter
def check_class(value):
    return str(type(value))


@register.filter
def retour_chariot(value):
    return f'{value}\n'


@register.filter
def capitalize(value: str):
    """
    même chose que string.capitalize()
    """
    return value.capitalize()


@register.filter
def sort_list_dict(list_dict, sort_key):
    """
    list_dict : liste de dictionnaires [{},{},... ]
    sort_key : string clef utilisée pour le triage de la liste
    """
    return sorted(list_dict, key=lambda x: x[sort_key], reverse=False)


@register.filter
def to_list(value):
    return list(value)


@register.filter
def is_valid_belgian_nrn(value):
    return misc.validate_belgian_nrn(unlazy(value))


@register.filter
def is_strong_authentication(session_user):
    """
    Anciennement town.py is_strong_authentication
    session_user : session_user
    return : (bool) True si le user est connecté avec une authentification forte
    """
    return bool('_niss' in session_user.verified_fields)


@register.filter
def authentication_delivrance_items_visibility(data_source, session_user):
    """
    Anciennement commune.py authentication_delivrance_items_visibility
    data_source : data source mode_de_delivrance
    session_user : session_user
    """
    if is_strong_authentication(session_user):
        for elm in data_source:
            if 'commune' not in elm['id']:
                # si connecté en authentification forte met l'item sans commune en id en disabled False
                elm['disabled'] = False
        return data_source
    return data_source


@register.filter
def is_agent(session_user):
    """
    Anciennement town.py is_agent
    session_user : session_user
    return : (bool) True si le user est autorisé à aller dans le backoffice.
    """
    return session_user.can_go_in_backoffice()


@register.filter
def get_roles(session_user):
    return session_user.get_roles()


@register.filter
def diff_dates(oldest_date, newest_date):
    """
    Anciennement town.py diff_dates
    oldest_date : datetime ou SafeText sous la forme "jj/mm/aaaa" ou "aaaa-mm-jj" ou "jj-mm-aaaa"
    newest_date : datetime ou SafeText sous la forme "jj/mm/aaaa" ou "aaaa-mm-jj" ou "jj-mm-aaaa"
    return : (str) nombre de jours de différence entre les 2 dates
    """

    def transform_date(fdate):
        fdate = fdate[:10]
        if '/' in fdate:
            return datetime.datetime.strptime(fdate, '%d/%m/%Y')
        if '-' in fdate:
            if len(fdate.split('-')[0]) == 4:
                return datetime.datetime.strptime(fdate, '%Y-%m-%d')
            if len(fdate.split('-')[0]) == 2:
                return datetime.datetime.strptime(fdate, '%d-%m-%Y')

    oldest_date = unlazy_date(oldest_date)
    newest_date = unlazy_date(newest_date)

    if type(oldest_date) in (SafeText, str):
        oldest_date = transform_date(oldest_date)
    if type(newest_date) in (SafeText, str):
        newest_date = transform_date(newest_date)
    diff = abs((oldest_date - newest_date).days)
    return str(diff)


@register.filter
def get_birthday_from_nn(numero_national):
    """
    numero_national : (str) numero national peut contenir espace . et -
    return : (datetime) la date de naissance
    """
    numero_national = clean_string(unlazy(numero_national))
    date = numero_national[:6]
    birthday = datetime.datetime.strptime(date, '%y%m%d')
    if birthday > datetime.datetime.now():
        birthday -= relativedelta(years=100)
    return birthday


@register.filter
def is_valid_tva_number(tva_number, formatage=True):
    """
    tva_number : (str) numéro de tva BE0000000097
    formatage : (bool) True/False
    return : (bool) True si le numéro est valide
    """
    # si formatage alors enlève les espaces, points et tirets
    if formatage:
        tva_number = clean_string(tva_number)

    if not tva_number[:2].upper() == 'BE':
        return False
    if not len(tva_number) == 12:
        if len(tva_number) == 11:
            tva_number = tva_number[:2] + '0' + tva_number[2:]
        else:
            return False
    if re.match(r'^\d{10}$', tva_number[2:]):
        int_value = int(tva_number[2:10])
        check_digit = int(int_value / 97) * 97
        if (97 - (int_value - check_digit)) == int(tva_number[10:12]):
            return True
        else:
            return False


@register.filter
def liste_type_rdv_plusieurs_personnes(value):
    """
    Anciennement liste_type_general_rdv.py
    value : (str) urls api liste des types de rendez-vous
    return : (list[str]) liste des différent type de rendez-vous sans - x personnes
    """
    value = value.strip()
    headers = {'Accept': 'application/json'}
    meetings_agendas = []
    for url in value.split('  '):
        meetings_agendas.append(requests.get(url, headers=headers).json())

    meetings_types = []
    for meetings_agenda in meetings_agendas:
        meetings_agenda['data'] = sorted(meetings_agenda['data'], key=lambda x: x['api']['datetimes_url'])
        meetings_types.extend(
            list(
                dict.fromkeys(
                    [
                        re.sub(r' (pour|-) [0-9]+ personne(s)?$', '', x['text'])
                        for x in meetings_agenda['data']
                    ]
                )
            )
        )
    return meetings_types


@register.filter
def liste_type_rdv_nombres_personnes(value, arg):
    """
    Anciennement liste_type_rdv.py
    value : (str) urls api liste des type de rendez-vous
    arg : (str) type de rendez-vous
    return : (list[str]) liste des rendez-vous de type arg
    """
    value = value.strip()
    headers = {'Accept': 'application/json'}
    meetings_agendas = []
    for url in value.split('  '):
        meetings_agendas.append(requests.get(url, headers=headers).json())

    meetings_types = []
    for meetings_agenda in meetings_agendas:
        meetings_types.extend([x for x in meetings_agenda['data'] if arg in x['text']])
    return meetings_types


@register.filter
def request_url_json(url):
    """
    Permet de get les données d'une url JSON
    url : (str) url du json
    return : {'data':[]}
    """
    headers = {'Accept': 'application/json'}

    return requests.get(url, headers=headers).json()


@register.filter
def stop_monday_for_we(value):
    """
    Empêche de prendre rdv le lundi matin pendant le w-e
    """
    headers = {'Accept': 'application/json'}
    meeting_liste = requests.get(value, headers=headers).json()
    today = datetime.datetime.today()
    weekday = today.weekday()
    if 4 >= weekday >= 0:
        return meeting_liste

    if 5 <= weekday <= 6:
        next_day = today
        while weekday != 0:
            next_day = next_day + datetime.timedelta(days=1)
            weekday = next_day.weekday()

        monday = next_day.replace(hour=12, minute=0, second=0, microsecond=0)
        return [
            x
            for x in meeting_liste
            if datetime.datetime.strptime(x['datetime'], '%Y-%m-%d %H:%M:%S') < monday
        ]

    if weekday > 6 or weekday < 0:
        raise Exception('Index OutOf weekday')


@register.filter
def calcul_total_abonnement_stationnement(fields_bloc):
    """
    Anciennement fields_bloc.py
    fields_bloc : (list[dict]) Bloc de champ Abonnements de stationnement données bénéficiaires
    return : (Decimal) addition des entrées price
    """
    total = 0

    for field in fields_bloc:
        for value in field.values():
            if isinstance(value, dict):
                total += Decimal(value.get('price'))

    return total


@register.filter
def autorisation_voyage_enfants_concernes(lst_enfants_concernes):
    """
    Anciennement autorisation_voyage_enfants_concernes.py
    lst_enfants_concernes : (list)
    return : (str)
    """
    enfants_concernes = ''
    if lst_enfants_concernes is not None:
        for e in lst_enfants_concernes:
            enfants_concernes = enfants_concernes + '-  ' + e[0] + ' né(e) à ' + e[1] + ' le ' + e[2] + '\r\n'

    return enfants_concernes


@register.filter
def distinct_list(value):
    """
    Permet de trier les doublons d'une liste
    value : (list)
    return : (list)
    """
    return list(set(value))


@register.filter
def comprehensive_list(liste: list, key: str):
    return [x[key] for x in liste]


@register.simple_tag
def replace_text(modele, *args):
    len_args = len(args)
    for i in range(0, len_args, 2):
        if args[i + 1] is not None:
            modele = modele.replace(unlazy(args[i]), unlazy(args[i + 1]))
    return modele


@register.filter
def json_loads(value):
    return json.loads(value)


##################
# PORTAIL MEMBRE #
##################
@register.filter
def randomize_int(start, end):
    """
    start : (int) début de la plage
    end : (int) fin de la plage
    return : (int) nombre aléatoire entre start et end
    """
    try:
        start = int(start)
        end = int(end)
    except Exception as e:
        return e
    return random.randint(start, end)


###################
# PORTAIL PARENTS #
###################
@register.filter
def get_plaines(data_source):
    """
    data_source : webservice.aes_get_available_plaines
    return : plaine available ?
    """
    return [
        activity for activities in [week['activities'] for week in data_source] for activity in activities
    ]


@register.filter
def get_navettes(url, plaines):
    """
    url : url de l'agenda des navettes
    plaines : json plaines response aes
    return : evenement navettes
    """
    plaines = unlazy_filter(plaines)
    if isinstance(plaines, str):
        plaines = json.loads(plaines)
    horaires = request_url_json(url)
    horaires = horaires['data']
    plaines_dates = [plaine['start_date'] for plaine in plaines]
    navettes = [horaire for horaire in horaires if horaire['date'] in plaines_dates]
    return navettes


#####################
# LOCATION DE SALLE #
#####################
@register.filter
def get_feature_details(data_source, feature):
    """
    data_source : json
    feature : json
    return : json
    """
    for i in data_source:
        if i.get('id') == feature.get('Type'):
            return i


@register.filter
def indisponibilites_for_a_date(date_debut_fin, indisponibilites):
    """
    date_debut_fin : str date_debut|date_fin
    indisponibilites : json
    """
    if date_debut_fin == '|':
        return []

    format_date_demande = '%d/%m/%Y'
    format_date_location = '%Y-%m-%dT%H:%M:%S'

    date_debut = datetime.datetime.strptime(str(date_debut_fin).split('|')[0], format_date_demande).date()
    date_fin = datetime.datetime.strptime(str(date_debut_fin).split('|')[1], format_date_demande).date()
    date_indisponibles = []

    for indisponibilite in indisponibilites['data']:
        StartDate = datetime.datetime.strptime(indisponibilite['StartDate'], format_date_location).date()
        EndDate = datetime.datetime.strptime(indisponibilite['EndDate'], format_date_location).date()
        if (
            StartDate <= date_debut <= EndDate
            or StartDate <= date_fin <= EndDate
            or date_debut <= StartDate <= date_fin
            or date_debut <= EndDate <= date_fin
        ):
            date_indisponibles.append(indisponibilite)

    return date_indisponibles


@register.simple_tag
def get_free_days_for_loan(start_date, end_date, room_loan):
    """

    :param start_date: date or str in format yyyy-mm-dd
    :param end_date: date or str in format yyyy-mm-dd
    :param room_loan: JSON or dictionary
    :return: list of dictionaly with text, id and disabled keys
    """

    def transform_atal_date(date_string):
        format_datetime = '%Y-%m-%dT%H:%M'

        return datetime.datetime.strptime(date_string[:16], format_datetime).date()

    # room_loan = unlazy_filter(room_loan)
    if isinstance(room_loan, str):
        room_loan = json.loads(room_loan)

    if isinstance(room_loan, dict):
        room_loan = room_loan['data']
    free_days = get_range_of_date(start_date, end_date)

    free_days_json = []
    try:
        for day in free_days:
            text = day.strftime('%d/%m/%Y')
            id = day.strftime('%Y-%m-%d')
            disabled = True in [
                (transform_atal_date(x['StartDate']) <= day <= transform_atal_date(x['EndDate']))
                for x in room_loan
            ]
            free_days_json.append({'text': text, 'id': id, 'disabled': disabled})

        return json.dumps(free_days_json)
    except:
        return 'erreur'


@register.filter
def get_range_of_date(date_debut, date_fin):
    """
    date_debut : str date_debut
    date_fin : str date_fin
    return : liste des dates entre date_debut et date_fin
    """

    def transform_date(fdate):
        fdate = fdate[:10]
        if '-' in fdate:
            return datetime.datetime.strptime(fdate, '%Y-%m-%d').date()

    date_debut = unlazy_filter(date_debut)
    date_fin = unlazy_filter(date_fin)
    if type(date_debut) in (SafeText, str):
        date_debut = transform_date(date_debut)
    if type(date_fin) in (SafeText, str):
        date_fin = transform_date(date_fin)
    delta = date_fin - date_debut
    return [date_debut + datetime.timedelta(days=x) for x in range(delta.days + 1)]


@register.filter
def condition_salle(date_heure_debut_fin, indisponibilites):
    """
    date_heure_debut_fin : str date_debutTheure_debut|date_finTheure_fin
    indisponibilites : json
    """
    if date_heure_debut_fin == 'T|T':
        return False

    format_date_heure_demande = '%d/%m/%YT%H:%M'
    format_date_heure_location = '%Y-%m-%dT%H:%M:%S'

    date_heure_debut_demande = datetime.datetime.strptime(
        str(date_heure_debut_fin).split('|')[0], format_date_heure_demande
    )
    date_heure_fin_demande = datetime.datetime.strptime(
        str(date_heure_debut_fin).split('|')[1], format_date_heure_demande
    )

    for indisponibilite in indisponibilites['data']:
        StartDate = datetime.datetime.strptime(indisponibilite['StartDate'], format_date_heure_location)
        EndDate = datetime.datetime.strptime(indisponibilite['EndDate'], format_date_heure_location)
        if (
            StartDate <= date_heure_debut_demande <= EndDate
            or StartDate <= date_heure_fin_demande <= EndDate
            or date_heure_debut_demande <= StartDate <= date_heure_fin_demande
            or date_heure_debut_demande <= EndDate <= date_heure_fin_demande
        ):
            return False

    return True


####################
# LIAISON IA.DELIB #
####################
@register.filter
def format_avis(avis):
    """
    avis : str texte long
    return : str
    """
    if avis not in (None, 'None', ''):
        avis_formate = '\n------------------------------------\n\n' + f'{avis}'
    else:
        avis_formate = ''
    return avis_formate


@register.filter
def format_liste_avis(avis):
    """
    avis : str texte long
    return : str
    """
    try:
        avis_formate = unlazy_filter(avis)
        avis_formate = re.sub(r'<br>|<br/>|</h3>', '\n', avis_formate)
        avis_formate = re.sub(r'<p>|</p>|<h3>', '', avis_formate)
    except Exception as e:
        return e
    return avis_formate


@register.simple_tag
def format_text(modele, *args):
    len_args = len(args)
    for i in range(0, len_args, 2):
        expression = '{{' + args[i] + '}}'
        if args[i + 1] is not None:
            modele = modele.replace(expression, unlazy(args[i + 1]))
    return modele


@register.simple_tag
def format_text_none(modele, *args):
    len_args = len(args)
    for i in range(0, len_args, 2):
        expression = '{{' + args[i] + '}}'
        rep = unlazy(args[i + 1])
        if rep is None:
            rep = ''
        modele = modele.replace(expression, str(rep))
    return modele


#########
# LIEGE #
#########


@register.filter
def conteneurs_verts(datasource, webservice):
    """
    datasource : (list) json
    webservice : (list) json
    return : (list) liste des conteneurs verts
    """
    webservice = webservice.get('data')
    if len(webservice) <= 1:
        for item in datasource[1:]:
            item['disabled'] = True
    else:
        datasource[0]['disabled'] = True
    return datasource


####################
# ABONNEMENT LIEGE #
####################
@register.filter
def somme_prix_bdc(bdc, key):
    """
    bdc : bloc de champs
    key : (str) champ du bloc de champs qui contient le prix
    si la key renvoie un dictionnaire il faut alors que la clef de celui-ci soit "prix" (mieux vaut d'ailleur utiliser
    |sum d'EO dans ce cas)
    return (int) la somme des prix du bloc de champs
    """
    bdc = unlazy(bdc)
    key = unlazy(key)

    somme = 0
    data = bdc['data'] if 'data' in bdc else bdc

    for champ in data:
        prix = champ[key]
        if isinstance(prix, dict):
            somme += int(prix['prix'])
        if isinstance(prix, (str, int)):
            somme += int(prix)

    return somme


@register.filter
def max_values(values):
    """
    values : (list) liste de valeur
    return la valeur la plus grande
    """
    return max(values)


@register.filter
def min_values(values):
    """
    values : (list) liste de valeur
    return la valeur la plus petite d'une liste
    """
    return min(values)


####################
# Python to Django #
####################


@register.filter
def simple_data_source(value):
    if not value:
        return 'No Value'

    data_source = unlazy_filter(value)

    try:
        if re.search(r"""\.split\(['"](.*)['"]\)""", data_source):
            data_source = re.sub(r"""\.split\(['"](.*)['"]\)""", r'|split:"\1"', data_source)
    except Exception as e:
        return e

    data_source = (
        """[{% for i in """
        + data_source
        + """ %}{"id": "{{ i }}", "text": "{{ i }}"}{% if not forloop.last %}, {% endif %}{% endfor %}]"""
    )

    return data_source


@register.filter
def add_json_dumps_data_source(value):
    def add_json_dumps(data_source):
        return f'{data_source}|json_dumps'

    def add_curly_braces(data_source):
        return '{{ ' + data_source + ' }}'

    if not value:
        return 'No Value'

    data_source = unlazy_filter(value)

    data_source = add_json_dumps(data_source)
    data_source = add_curly_braces(data_source)

    return data_source


@register.filter
def add_unlazy_filter_json_dumps(value):
    def add_unlazy(data_source):
        return f'{data_source}|unlazy_filter'

    def add_json_dumps(data_source):
        return f'{data_source}|json_dumps'

    def add_curly_braces(data_source):
        return '{{ ' + data_source + ' }}'

    if not value:
        return 'No Value'

    data_source = unlazy_filter(value)

    data_source = add_unlazy(data_source)
    data_source = add_json_dumps(data_source)
    data_source = add_curly_braces(data_source)

    return data_source


@register.filter
def webservice_data_source(value):
    if not value:
        return 'No Value'

    data_source = unlazy_filter(value)

    data_source = (
        '[{ % for i in  '
        + data_source
        + ' %}{{ i|json_dumps }}{% if not forloop.last %}, {% endif %}{% endfor %}]'
    )

    return data_source


@register.filter
def dict_data_source(value):
    """
    value : (str) data_soure name
    """
    if not value:
        return 'No Value'

    def dict_to_get(data_source):
        data_source = data_source.replace('[', '|get:')
        data_source = data_source.replace(']', '')
        return data_source

    def add_unlazy(data_source):
        return f'{data_source}|unlazy_filter'

    def add_json_dumps(data_source):
        return f'{data_source}|json_dumps'

    def add_curly_braces(data_source):
        return '{{ ' + data_source + ' }}'

    data_source = unlazy_filter(value)
    if '[' in data_source:
        data_source = dict_to_get(data_source)

    data_source = add_unlazy(data_source)

    data_source = add_json_dumps(data_source)

    data_source = add_curly_braces(data_source)

    return data_source
