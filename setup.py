#! /usr/bin/env python

import os
import subprocess
import sys
from distutils.cmd import Command
from distutils.command.build import build as _build
from distutils.command.sdist import sdist

from setuptools import find_packages, setup
from setuptools.command.install_lib import install_lib as _install_lib


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        with open('VERSION', 'w') as fd:
            fd.write(version)
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class build(_build):
    pass


class install_lib(_install_lib):
    pass


setup(
    name='imio-teleservices-templatetags',
    version=get_version(),
    description='Extra template tags for iMio teleservices',
    author='Frederic Peters',
    author_email='fpeters@entrouvert.com',
    packages=find_packages(),
    include_package_data=True,
    url='https://dev.entrouvert.org/projects/combo/',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    install_requires=[],
    zip_safe=False,
    cmdclass={
        'build': build,
        'install_lib': install_lib,
        'sdist': eo_sdist,
    },
)
