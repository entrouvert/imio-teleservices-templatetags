import os

import pytest

from wcs.qommon.http_request import HTTPRequest
from wcs.qommon.template import Template

from .utilities import clean_temporary_pub, create_temporary_pub


@pytest.fixture
def pub():
    pub = create_temporary_pub()
    pub.substitutions.feed(pub)
    req = HTTPRequest(None, {'SCRIPT_NAME': '/', 'SERVER_NAME': 'example.net'})
    pub.set_app_dir(req)
    return pub


def teardown_module(module):
    clean_temporary_pub()


def test_nrn(pub):
    tmpl = Template('{% if value|is_valid_belgian_nrn %}yes{% else %}no{% endif %}')
    assert tmpl.render({'value': '85073003328'}) == 'yes'
    assert tmpl.render({'value': '85073003329'}) == 'no'


def test_strong_authentication(pub):
    tmpl = Template('{% if session_user|is_strong_authentication %}yes{% else %}no{% endif %}')
    user = pub.user_class(name='foo bar')
    assert tmpl.render({'session_user': user}) == 'no'
    user.verified_fields = ['_niss']
    assert tmpl.render({'session_user': user}) == 'yes'
