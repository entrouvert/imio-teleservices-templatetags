TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'en-us'
ALLOWED_HOSTS = ['*']

TEMPLATES[0]['OPTIONS']['builtins'].append('imio_teleservices_templatetags.templatetags.imio_teleservices')
